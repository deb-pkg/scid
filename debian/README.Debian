scid for Debian
---------------

 * The latest spell check files, player data and photo library

   You can maintain your collected chess databases with player information 
   and display photos of international players in scid. This data can be 
   found at: http://scid.sourceforge.net/download.html

 * Opening book support

   Scid has support for opening books, which are made with the winboard/UCI 
   polyglot protocol adaptor. The binary books that are usually shipped with 
   the sources of scid had  to be removed from debian because of policy reasons.
   You can make some yourself with some hints from www.linuxchess.org or take 
   Marc Lacrosses performance.bin and varied.bin from: 
   http://wbec-ridderkerk.nl/html/download.htm


 * Additional eco files
   
   These files are already installed for french and spanish as:
   /usr/share/scid/data/scid-esp.eco and scid_fr.eco

 * The former html documentation
   
   This documentation is now available through the internal TCL/TK help 
   browser and was removed by upstream developers. It was outdated anyway.

 -- Oliver Korff <ok@xynyx.de>  Sat, 01 Dec 2007 09:51:08 +0100
 
 * Alioth

   The Debian package for Scid has its own CVS repository on Alioth: see 
   <http://alioth.debian.org/projects/pkg-scid>.

 * Zlib compression

   The upstream source of Scid includes the zlib compression library. Since
   that is packaged separately in Debian, this version of Scid does not
   use the included zlib. Instead, it links to the version of the zlib
   compression library that is already present. It has been removed from
   the .orig.tar.gz-file.

 * Endgame tablebases

   Scid can be compiled with support for endgame tablebases. The tablebase
   routines used in Scid are, however, not freely distributed in the sense
   dictated by Debian guidelines. Therefore, tablebase support is not 
   present in the Debian version of Scid. In fact, the source of the
   tablebase routines has even been removed from the .orig.tar.gz-file.

 * LaTeX

   If you want to use Scid's LaTeX exporting capabilities, you need
   Piet Tutelaers' chess package. In Debian, this is available as
   the packages texlive-games.

 -- Peter van Rossum <petervr@debian.org>  Sun, 21 Dec 2003 18:43:13 -0800
