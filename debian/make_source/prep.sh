#/bin/bash
# #########################
# This script automagically builds a new scid package and 
# prepares the sources for use in debian. It is only necessary
# to have a scid directory, with the sources, this script and 
# the Makefile.conf patch. Additional we need the debian directory 
# placed in the $CURDIR, it can be copied from the debian sources
# ########################
# Getting the sources:
# cvs -d:pserver:anonymous@scid.cvs.sourceforge.net:/cvsroot/scid login 
#
# cvs -z3 -d:pserver:anonymous@scid.cvs.sourceforge.net:/cvsroot/scid co -P scid
#
###############
###   Settings
###############

CURDIR="$HOME/debian/scid"
NAME="scid"
EPOCH="1:"
VERSION="4.5.1.cvs"`date +%Y%m%d`
DESTDIR=$NAME-$VERSION
DEBIAN_VERSION=""

###############
### end Settings
##############

# Preparations to have a new source tree
		echo "DESTDIR=$DESTDIR"
		echo "CURDIR=$CURDIR"
		rm -rf $DESTDIR
		mkdir $DESTDIR
		mkdir -p $CURDIR/tmp
cd scid-code

cd $CURDIR
		cp scid-code/* $CURDIR/$DESTDIR/ 
#################################################
# Prepare the needed directories in the source
# We do not need any engines, they have native packages
# We do not need the pocket stuff, because we won't compile for 
# mobile devices
# We do not need the CVS Tags in the source
# The tablebase Code is non-free, so it is removed here
# Sorry also the books are considered non-free
# I also remove the zlib sources, they come out of debian anyway
#################################################

		DIRS=`find $CURDIR/scid-code -maxdepth 1 -type d | egrep -v "engines|pocket|CVS"`

# Copy all files and dirs into place

		for i in $DIRS; do
			CDIR=`basename $i`
			mkdir "$CURDIR/$DESTDIR/$CDIR"; 
			cp -r "scid-code/$CDIR" "$CURDIR/$DESTDIR/" ; 
		done
		rm -rf $CURDIR/$DESTDIR/scid/
#		rm -f $CURDIR/$DESTDIR/books/*
		for i in $(find $CURDIR/$DESTDIR/src/egtb/ -type f); do echo > $i ; done
		rm -f $CURDIR/$DESTDIR/src/zlib/*


# CVS Files that should not get into debians orig.tar.gz

cd $DESTDIR 
		find . -name CVS -exec rm -rf {} \; 2>/dev/null
		rm -rf .git
		chmod 644 sounds/*
cd $CURDIR


#################################################
# Patch the Makefile.conf to make configure possible
# and configure the sources to have a Makefile
# any other patching is done with the dpatch system
#################################################
# cd $CURDIR 
#		cp Makefile.conf $DESTDIR/
# cd $DESTDIR 
# 		./configure
# cd $CURDIR

# Remove any already existing tar.gz and directories
# and create the orig.tar.gz

		rm "$NAME"_"$VERSION".orig.tar.gz
		tar -cvzpf "$NAME"_"$VERSION".orig.tar.gz $DESTDIR $2>/dev/null

# copy the debian directory and the scripts to the source tree
		cd $CURDIR/tmp
		rm -rf $CURDIR/tmp/scid*
		apt-get source scid
		DEBIAN_VERSION=$(ls -d scid-*)
		echo $DEBIAN_VERSION
		mv -v $DEBIAN_VERSION/debian $CURDIR/$DESTDIR/
		cd $CURDIR
		cp prep.sh $DESTDIR/debian/make_source

echo "###########################################"
echo "BE SURE TO EDIT DEBIAN/changelog or dch -i"
echo "###########################################"

cd $DESTDIR
dch -v $EPOCH$VERSION-1 New Upstream Version.

# cd $DESTDIR/ 
# 		dpkg-buildpackage -uc -us -rfakeroot
